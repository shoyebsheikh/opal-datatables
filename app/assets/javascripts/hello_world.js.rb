Element.expose :DataTable
Document.ready? do
  token     = Element['meta[name=csrf-token]'].attr('content');
  settings  = {
      "ajax": {
        "url": "/data.json",
        "type": 'POST',
        "beforeSend": lambda do |xhr|
          `xhr.setRequestHeader('X-CSRF-Token', token)`
        end 
      },
      "pagingType": "full_numbers",
      "destroy": true,
    "columns": [
      ["data": "Name"],
      ["data": "Position"],
      ["data": "Office"],
      ["data": "Extn."],
      ["data": "Start"],
      ["data": "Salary"]
    ]
    }
  Element['#organizations-datatable'].DataTable(settings.to_n)

end
