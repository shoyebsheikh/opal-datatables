Rails.application.routes.draw do
  get 'source/index'
  get 'hello_world/index'
  post '/data.json', :to => redirect('/data.json')
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
